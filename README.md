# Okeenea BeaconsTrigger SDK

## Installation

### CocoaPods

```ruby
pod 'OkeeneaBeaconsTrigger'
```

## Usage

**Pre-requisite** :

- Ask for Location authorization (to range CLBeacons).
- Ask for Bluetooth authorization (to scan for CBPeripherals).

The SDK will not ask for these authorizations, it will just check them and report errors.

### Note about completion handlers

The completion handler informs when communication with the beacon has been done. **It does not tell that the beacon did finish playing a message.**

You can use it to report any communication error to the user or to know that the action (ex: trigger) did succeed.

### Demo app

You can try the SDK using the demo app in `Example/OkeeneaBeaconsTriggerDemo`.

### Swift

```swift
// Initialize the SDK
let beaconsTrigger = BeaconsTrigger()

// Start scan
beaconsTrigger.startScan(onUpdate: { (beacons: [OkeeneaBeacon]) in
    // usually called once per second with updated found beacons (name updates, range updates, etc.)
}, onError: { (error: OkeeneaError) in
    // handle the error
})

// Stop scan
beaconsTrigger.stopScan()
```

```swift
public class OkeeneaError: NSError {
}

@objc
public enum OkeeneaErrorCodes: Int {
    case locationUnauthorized
    case trackingError
    case other
}
```

```swift
@objc
protocol OkeeneaBeacon {
    
    /// Id of the beacon
    var id: String { get }
    
    /// Name of the beacon
    var name: String { get }
    
    /// Proximity's information
    var proximity: BeaconProximity { get }
    
    /// Beacon's kind
    var kind: BeaconKind { get }
    
    /// Trigger the beacon
    func trigger(completionHandler: @escaping OkeeneaBeaconCompletionHandler)
}

typealias OkeeneaBeaconCompletionHandler = (Error?) -> Void

public class BeaconProximity: NSObject {
    
    @objc
    public init(timestamp: Date? = nil, rssi: Int = 0, proximity: CLProximity = .unknown, accuracy: CLLocationAccuracy = -1)
    
    @objc
    public let timestamp: Date?
    @objc
    public let rssi: Int
    @objc
    public let proximity: CLProximity
    @objc
    public let accuracy: CLLocationAccuracy
}

@objc
public enum BeaconKind: Int {
    case pointOfInterest
    case audiblePedestrianSignal
}
```
