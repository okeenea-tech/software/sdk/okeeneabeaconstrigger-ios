// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 6.0.3 effective-5.10 (swiftlang-6.0.3.1.10 clang-1600.0.30.1)
// swift-module-flags: -target arm64-apple-ios12.4 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -enable-bare-slash-regex -module-name OkeeneaBeaconsTrigger
// swift-module-flags-ignorable: -no-verify-emitted-module-interface
import AVFoundation
import CoreBluetooth
import CoreLocation
import Foundation
@_exported import OkeeneaBeaconsTrigger
import RswiftResources
import Swift
import SwifterSwift
import UIKit
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
public enum BeaconTrackingError : Swift.Error, Swift.Equatable {
  case settingsIssue(reason: Swift.String, recoverySuggestion: Swift.String)
  case other(Swift.String)
  public var reason: Swift.String {
    get
  }
  public var recoverySuggestion: Swift.String {
    get
  }
  public static func == (a: OkeeneaBeaconsTrigger.BeaconTrackingError, b: OkeeneaBeaconsTrigger.BeaconTrackingError) -> Swift.Bool
}
@objc public enum OkeeneaErrorCodes : Swift.Int {
  case locationUnauthorized = 1
  case trackingError = 2
  case other = 3
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc @_inheritsConvenienceInitializers public class OkeeneaError : Foundation.NSError, @unchecked Swift.Sendable {
  @objc override dynamic public var hash: Swift.Int {
    @objc get
  }
  @objc override dynamic public func isEqual(_ object: Any?) -> Swift.Bool
  @objc override dynamic public init(domain: Swift.String, code: Swift.Int, userInfo dict: [Swift.String : Any]? = nil)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
@objc public protocol OkeeneaBeacon {
  @objc var id: Swift.String { get }
  @objc var name: Swift.String { get }
  @objc var proximity: OkeeneaBeaconsTrigger.BeaconProximity { get }
  @objc var kind: OkeeneaBeaconsTrigger.BeaconKind { get }
  @objc func trigger(completionHandler: @escaping OkeeneaBeaconsTrigger.OkeeneaBeaconCompletionHandler)
}
public typealias OkeeneaBeaconCompletionHandler = ((any Swift.Error)?) -> Swift.Void
@objc public class BeaconProximity : ObjectiveC.NSObject {
  @objc public init(timestamp: Foundation.Date? = nil, rssi: Swift.Int = 0, proximity: CoreLocation.CLProximity = .unknown, accuracy: CoreLocation.CLLocationAccuracy = -1)
  @objc final public let timestamp: Foundation.Date?
  @objc final public let rssi: Swift.Int
  @objc final public let proximity: CoreLocation.CLProximity
  @objc final public let accuracy: CoreLocation.CLLocationAccuracy
  @objc override dynamic public var hash: Swift.Int {
    @objc get
  }
  @objc override dynamic public func isEqual(_ object: Any?) -> Swift.Bool
  @objc deinit
}
@objc public enum BeaconKind : Swift.Int {
  case pointOfInterest
  case audiblePedestrianSignal
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
#if compiler(>=5.3) && $RetroactiveAttribute
extension Swift.String : @retroactive Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
}
#else
extension Swift.String : Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
}
#endif
@objc public enum LanguagePreference : Swift.Int, Swift.CaseIterable, Swift.CustomDebugStringConvertible {
  case french
  case english
  case spanish
  case german
  case dutch
  case portuguese
  case italian
  case chinese
  case japanese
  case korean
  case vietnamese
  case russian
  case swedish
  case danish
  case finnish
  case norwegian
  case arabic
  case turkish
  case greek
  case indonesian
  case malay
  case thai
  case hindi
  case hungarian
  case polish
  case czech
  case slovak
  case ukrainian
  case catalan
  case romanian
  case croatian
  case hebrew
  public var isoLanguageCode: Swift.String {
    get
  }
  public var localizedName: Swift.String {
    get
  }
  public static var devicePreferredLanguage: OkeeneaBeaconsTrigger.LanguagePreference {
    get
  }
  public static func fromStringOrDefault(_ rawValue: Swift.String?) -> OkeeneaBeaconsTrigger.LanguagePreference
  public var debugDescription: Swift.String {
    get
  }
  public init?(rawValue: Swift.Int)
  public typealias AllCases = [OkeeneaBeaconsTrigger.LanguagePreference]
  public typealias RawValue = Swift.Int
  nonisolated public static var allCases: [OkeeneaBeaconsTrigger.LanguagePreference] {
    get
  }
  public var rawValue: Swift.Int {
    get
  }
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers public class BeaconsTrigger : ObjectiveC.NSObject {
  @objc convenience override dynamic public init()
  @objc public func startScan(onUpdate: @escaping ([any OkeeneaBeaconsTrigger.OkeeneaBeacon]) -> Swift.Void, onError: @escaping (OkeeneaBeaconsTrigger.OkeeneaError) -> Swift.Void)
  @objc public func stopScan()
  @objc deinit
}
public protocol AnalyticsTracker {
  func track(event: OkeeneaBeaconsTrigger.AnalyticsEvent)
}
public struct AnalyticsEvent : Swift.Equatable {
  public var name: OkeeneaBeaconsTrigger.AnalyticsEvent.Name
  public var properties: [Swift.String : Any]
  public static func == (lhs: OkeeneaBeaconsTrigger.AnalyticsEvent, rhs: OkeeneaBeaconsTrigger.AnalyticsEvent) -> Swift.Bool
  public static func stopBusGiveFeedback() -> OkeeneaBeaconsTrigger.AnalyticsEvent
  public enum Name : Swift.String {
    case stopBusGiveFeedback
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
}
extension OkeeneaBeaconsTrigger.OkeeneaErrorCodes : Swift.Equatable {}
extension OkeeneaBeaconsTrigger.OkeeneaErrorCodes : Swift.Hashable {}
extension OkeeneaBeaconsTrigger.OkeeneaErrorCodes : Swift.RawRepresentable {}
extension OkeeneaBeaconsTrigger.BeaconKind : Swift.Equatable {}
extension OkeeneaBeaconsTrigger.BeaconKind : Swift.Hashable {}
extension OkeeneaBeaconsTrigger.BeaconKind : Swift.RawRepresentable {}
extension OkeeneaBeaconsTrigger.LanguagePreference : Swift.Equatable {}
extension OkeeneaBeaconsTrigger.LanguagePreference : Swift.Hashable {}
extension OkeeneaBeaconsTrigger.LanguagePreference : Swift.RawRepresentable {}
extension OkeeneaBeaconsTrigger.AnalyticsEvent.Name : Swift.Equatable {}
extension OkeeneaBeaconsTrigger.AnalyticsEvent.Name : Swift.Hashable {}
extension OkeeneaBeaconsTrigger.AnalyticsEvent.Name : Swift.RawRepresentable {}
