//
//  OkeeneaBeaconsTrigger.h
//  OkeeneaBeaconsTrigger
//
//  Created by Nicolas VERINAUD on 04/06/2024.
//  Copyright © 2024 Okeenea. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for OkeeneaBeaconsTrigger.
FOUNDATION_EXPORT double OkeeneaBeaconsTriggerVersionNumber;

//! Project version string for OkeeneaBeaconsTrigger.
FOUNDATION_EXPORT const unsigned char OkeeneaBeaconsTriggerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OkeeneaBeaconsTrigger/PublicHeader.h>


