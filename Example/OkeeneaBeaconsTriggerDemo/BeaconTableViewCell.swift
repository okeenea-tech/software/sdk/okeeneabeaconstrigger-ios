//
//  Copyright © 2024 Okeenea. All rights reserved.
//

import OkeeneaBeaconsTrigger
import UIKit

class BeaconTableViewCell: UITableViewCell {
    
    override init(style: CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func updated(with beacon: OkeeneaBeacon) -> Self {
        textLabel?.text = beacon.name
        detailTextLabel?.text = "\(beacon.id) (rssi=\(beacon.proximity.rssi))"
        return self
    }
}
